import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'
import { ApiService } from '../api.service'

@Component({
  selector: 'app-dealer-edit',
  templateUrl: './dealer-edit.component.html',
  styleUrls: ['./dealer-edit.component.scss']
})
export class DealerEditComponent implements OnInit {

  private id : string  = null;
  private psub : any = null;
  private info : any = [];

  constructor(private route: ActivatedRoute,protected location : Location,protected api : ApiService)
  {
  }

  ngOnInit() {
    this.psub = this.route.params.subscribe((params)=>{
      this.id = params.id;
      this.api.dealer.get(this.id).then((info)=>{
        this.info = info;
      },(reason)=>{
        console.log('Could\'nt get dealer from api because : %s',reason);
      })
    })
  }

  updateInfo()
  {
    this.api.dealer.update(this.id,this.info.name,this.info.connection.server,this.info.connection.user,this.info.connection.password,this.info.connection.database)
    .then(()=>{
      this.location.back();
    })
  }
}
