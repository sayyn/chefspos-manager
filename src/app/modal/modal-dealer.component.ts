import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ApiService } from '../api.service'



@Component({
  selector: 'app-modal-dealer',
  templateUrl: './modal-dealer.component.html',
  styleUrls: ['./modal-dealer.component.scss']
})
export class ModalDealerComponent implements OnInit {

  dealers : any;


  constructor(public dialogRef: MatDialogRef<ModalDealerComponent>,@Inject(MAT_DIALOG_DATA) public data: any,protected api : ApiService){
     this.data.existings = this.data.existings.map(function(e) { return e._id; })
  }

  ngOnInit() {
    this.api.dealer.all.then((result)=>{
      this.dealers = result.filter((item)=>{
         item.selected = false;
         return this.data.existings.indexOf(item._id) === -1;
      })
    })
  }

  


  finish(): void
  {   
    let selecteds = this.dealers.filter((item)=>{
      if(item.selected)
      {
        item.selected = false;
        return true;
      }
     
      return false;
    })
    this.dialogRef.close(selecteds);
    
  }

}
