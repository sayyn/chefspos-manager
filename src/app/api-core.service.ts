import { Injectable } from '@angular/core';
import { Http,RequestOptions,Headers,Response,URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';

@Injectable()
export class ApiCoreService {

  public base : string = 'https://protected-fortress-98173.herokuapp.com'
  public token : string  = null;

  constructor(public http: Http)
  {

  }

   /**
   * Get Request
   * @param url request url
   * @param params  request extra params
   */
  get(url : string,params? : any) : Promise<any>{
    let searchParams = new URLSearchParams();
    _.forOwn(params, (value, key) => { searchParams.set(key, value) });
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : "Basic "+this.token });
    let options = new RequestOptions({ headers: headers, search: searchParams });
    return this.http.get(this.url(url), options).map((res:Response)=>res.json()).toPromise<any>()
  }

  /**
   * Post Request
   * @param url request url
   * @param params request extra params
   */
  post(url : string,params? : any) : Promise<any>{
    let bodyString = JSON.stringify(params);
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : "Basic "+this.token });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.url(url),bodyString,options).map((res:Response)=>res.json()).toPromise<any>()
  }

  /**
   * Put request
   * @param url request url
   * @param params request extra params
   */
  put(url : string,params? : any) : Promise<any>{
    let bodyString = JSON.stringify(params);
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : "Basic "+this.token });
    let options = new RequestOptions({ headers: headers, body: bodyString });
    return this.http.put(this.url(url),bodyString,options).map((res:Response)=>res.json()).toPromise<any>()
  }

  /**
   * Delete request
   * @param url request url
   */
  delete(url : string) : Promise<any>{
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : "Basic "+this.token });
    let options = new RequestOptions({ headers: headers});
    return this.http.delete(this.url(url),options).map((res:Response)=>res.json()).toPromise<any>()
  }

  
  private url(url : string){
    return this.base + url;
  }

}
