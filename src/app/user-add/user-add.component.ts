import { Component, OnInit } from '@angular/core';;
import { MatDialog } from '@angular/material';

import { ApiService } from '../api.service'
import { ModalComponent } from '../modal/modal.component'
import { ModalDealerComponent } from '../modal/modal-dealer.component'

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  private info : any = {};
  private dealerAdd : boolean = false;
  


  /**
   * Form fields
   */
  private sqlname = null;
  private sqlserver = null;
  private sqlusername = null;
  private sqlpassword = null;
  private sqldatabase = null;

  constructor(protected api : ApiService,public dialog: MatDialog)
  {}


  ngOnInit() {
  }

  ngAfterContentChecked()
  {
    
  }


  addDealer(){
    this.api.dealer.add(this.info._id,this.sqlname,this.sqlserver,this.sqlusername,this.sqlpassword,this.sqldatabase)
    .then((result)=>{
      this.info.dealers.unshift(result);
      this.sqldatabase = null;
      this.sqlname = null;
      this.sqlusername = null;
      this.sqlserver = null;
      this.sqlpassword = null;
    })
  }

  excludeDealer(dealer : any){


    this.dialog.open(ModalComponent,{
      data : {
         title : 'Are you  sure ?',
         message : 'Do you want to continue?'
      }
    }).afterClosed().subscribe((result)=>{
      if(result){
        this.api.dealer.exclude(this.info._id,dealer._id)
        .then((result)=>{
          this.info.dealers.splice(this.info.dealers.indexOf(dealer),1)
        })
      }
    })

  }

  chooseExisting()
  {
    this.dialog.open(ModalDealerComponent,{
      data : {
         title : 'Choose existing',
         existings : this.info.dealers
      }
    }).afterClosed().subscribe((result)=>{
      if(result){ 
        this.api.dealer.assignMany(this.info._id,result).then(()=>{
          this.info.dealers = this.info.dealers.concat(result)
        })
      }
    })
  }

  register()
  {
    this.api.user.add({
      name : this.info.name,
      username : this.info.username,
      password : this.info.password
    })
    .then((info)=>{

      this.info = info
      

    },(reason)=>{
      this.dialog.open(ModalComponent,{
        data : {
           title : 'Uh oh!',
           message : JSON.parse(reason._body).error,
           alert : true ,
        }
      })
      console.log(reason);
    })
  }

}
