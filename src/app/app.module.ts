import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes }   from '@angular/router';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { MatButtonModule,MatDialogModule,MatInputModule,MatTableModule,MatListModule,MatSlideToggleModule } from '@angular/material';

import { ApiCoreService } from './api-core.service'
import { ApiService } from './api.service'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ModalComponent } from './modal/modal.component';
import { ModalDealerComponent } from './modal/modal-dealer.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { DealerEditComponent } from './dealer-edit/dealer-edit.component';
import { UserAddComponent } from './user-add/user-add.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'users', component: UsersComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full'},  
  { path: 'users/add', component: UserAddComponent },
  { path: 'users/:id/edit', component: UserEditComponent },
  { path: 'dealers/:id/edit', component: DealerEditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    UsersComponent,
    NavbarComponent,
    ModalComponent,
    UserEditComponent,
    DealerEditComponent,
    ModalDealerComponent,
    UserAddComponent,
  ],
  entryComponents:[
    NavbarComponent,
    ModalComponent,
    ModalDealerComponent
  ],
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    HttpModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers : [
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatSlideToggleModule,
    ApiCoreService,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
