import { Injectable } from '@angular/core';
import { ApiCoreService } from './api-core.service';
import { Http } from '@angular/http';


@Injectable()
export class ApiService extends ApiCoreService{

  constructor(public http : Http)
  {
    super(http);
  }



  
  user = {
    all : this.get('/api/users'),
    add : (params : any) : Promise<any> =>{
      return this.post('/api/users', params)
    },
    delete : (userid : string) : Promise<any> => {
      return this.delete('/api/users/'+userid)
    },
    update : (userid : any, params : any) : Promise<any> =>{
      return  this.post('/api/users/'+userid,params)
    },
    info : (userid : any) : Promise<any> => {
      return  this.get('/api/users/'+userid)
    }
  }

  dealer = {

    all : this.get('/api/dealers'),
    get : (dealerid : any) : Promise<any> =>{
      return this.get('/api/dealers/'+dealerid);
    },
    add : (userid : string,sqlname : string,sqlserver : string,sqlusername : string, sqlpassword : string, sqldatabase) : Promise<any> =>{
     return this.post('/api/users/'+userid+'/dealers',{
        name : sqlname,
        user : sqlusername,
        password : sqlpassword,
        server : sqlserver,
        database  : sqldatabase
      })
    },
    assignMany : (userid : string, dealers : any) : Promise<any> =>{
      return this.put('/api/users/'+userid+'/dealers', { dealers : dealers})
    },
    exclude : (userid : string,dealerid : string) : Promise<any> =>{
      return this.delete('/api/users/'+userid+'/dealers/'+dealerid);
    },
    update : (dealerid : string,name,server,username,password,database) : Promise<any> =>{
      return this.post('/api/dealers/'+dealerid,{
        name : name,
        username : username,
        password : password,
        server : server,
        database  : database
      });
    }
  }


}
