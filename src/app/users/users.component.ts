import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../api.service'


import { ModalComponent } from '../modal/modal.component'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit,AfterViewInit {


  users : Array<any> = new Array<any>();

  constructor(public dialog: MatDialog,protected api : ApiService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.api.user.all.then((users)=>{
      this.users = users;
    },(reason)=>{
      console.log('Could\'nt get users from api because : %s',reason);
    })
  }


  


  deleteUser(user : any)
  {

      this.dialog.open(ModalComponent,{
        data : {
           title : 'Are you  sure ?',
           message : 'Once deleted, you could\'nt bring user back'
        }
      }).afterClosed().subscribe((result)=>{
        if(result){

          this.api.user.delete(user._id).then(()=>{
            this.users.splice(this.users.indexOf(user),1) 
          })
          
        }
      })
    
  }
}
