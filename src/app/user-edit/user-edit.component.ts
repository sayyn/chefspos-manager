import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ApiService } from '../api.service'
import { ModalComponent } from '../modal/modal.component'
import { ModalDealerComponent } from '../modal/modal-dealer.component'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {


  private id : string  = null;
  private psub : any = null;
  private info : any = [];

  private dealerAdd : boolean = false;



  /**
   * Form fields
   */
  private sqlname = null;
  private sqlserver = null;
  private sqlusername = null;
  private sqlpassword = null;
  private sqldatabase = null;

  constructor(private route: ActivatedRoute,protected api : ApiService,public dialog: MatDialog)
  {}

  ngOnInit() {
    this.psub = this.route.params.subscribe((params)=>{
      this.id = params.id;
      this.api.user.info(this.id).then((info)=>{
        this.info = info;
      },(reason)=>{
        console.log('Could\'nt get users from api because : %s',reason);
      })
    })
  }

  ngOnDestroy(){
    this.psub.unsubscribe();
  }

  addDealer(){
    this.api.dealer.add(this.id,this.sqlname,this.sqlserver,this.sqlusername,this.sqlpassword,this.sqldatabase)
    .then((result)=>{
      this.info.dealers.unshift(result);
      this.sqldatabase = null;
      this.sqlname = null;
      this.sqlusername = null;
      this.sqlserver = null;
      this.sqlpassword = null;
    })
  }

  excludeDealer(dealer : any){


    this.dialog.open(ModalComponent,{
      data : {
         title : 'Are you  sure ?',
         message : 'Do you want to continue?'
      }
    }).afterClosed().subscribe((result)=>{
      if(result){
        this.api.dealer.exclude(this.id,dealer._id)
        .then((result)=>{
          this.info.dealers.splice(this.info.dealers.indexOf(dealer),1)
        })
      }
    })

  }

  chooseExisting()
  {
    this.dialog.open(ModalDealerComponent,{
      data : {
         title : 'Choose existing',
         existings : this.info.dealers
      }
    }).afterClosed().subscribe((result)=>{
      if(result){ 
        this.api.dealer.assignMany(this.id,result).then(()=>{
          this.info.dealers = this.info.dealers.concat(result)
        })
      }
    })
  }

  updateInfo()
  {
    this.api.user.update(this.id,{
      name : this.info.name,
      username : this.info.username,
      password : this.info.password
    })
    .then((info)=>{

      this.info.username = info.username;
      this.info.password = info.password;
      this.info.name = info.name;

    },(reason)=>{
      console.log('Could\'nt get users from api because : %s',reason);
    })
  }

}
